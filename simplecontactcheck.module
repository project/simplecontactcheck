<?php
/**
* @file
* Let users subscribe to newsletters from the contact form. 
*
* A small add-on module that works with the simplenews module to allow you 
* to add checkboxes for newsletter susbscriptions to the contact form within Drupal.
*/


/**
* Validate the form.
*/
function simplecontactcheck_simplenews_admin_settings_validate($form_id, $form_values) {
  if (strlen($form_values['wording']) > 60) {
    // We notify the form API that this field has failed validation.
    form_set_error('user_name', t('The wording sentence is too long.'));
  }
}

function simplecontactcheck_contact_submit($form_id, $form_values) {
  //DEBUG drupal_set_message(var_export($form_values['simplecontactcheck_contact_form_newsletter'], true));
  
  // reverse the opt-out check values
  $newsletters_as_optIn = array();
  foreach ($form_values['simplecontactcheck_contact_form_newsletter'] as $aCheckboxKey => $aCheckboxValue) {
    if ($aCheckboxKey < 0) {
  	  $newsletters_as_optIn[abs($aCheckboxKey)] = abs($aCheckboxValue);//don't reverse opt-out = $aCheckboxValue - $aCheckboxKey;
    } else {
      $newsletters_as_optIn[$aCheckboxKey] = $aCheckboxValue;
    }
  }
  
  //DEBUG drupal_set_message(var_export($newsletters_as_optIn, true));
  
  $newsletters_wanna_subscribe = array_filter($newsletters_as_optIn); // remove newsletters don't want to subscribe
  foreach ($newsletters_wanna_subscribe as $tid) {
    $newsletter = taxonomy_get_term($tid);
    simplenews_subscribe_user($form_values['mail'], $newsletter->tid, FALSE);
  }

  // Only display message when there are newsletters subscribed.
  if (sizeof($newsletters_wanna_subscribe) >= 1) {
    drupal_set_message(t('Your newsletter subscription options have been saved.'));
  }
}



/**
* Implementation of hook_perm().
*/
function simplecontactcheck_perm() {
  return array('view contact checkboxes');
}

function simplecontactcheck_form_alter($form_id, &$form) {
  // Inject HTML to Contact page, and wire form submit process function
  if (($form_id == 'contact_mail_page') && user_access('view contact checkboxes')) {	
    // override the form submit process function while still execute original submit process
    // by appending function name to original array
	  $form['#submit']['simplecontactcheck_contact_submit'] = array();
    
    // get setting data from DB
    $sql = "SELECT * FROM {simplecontactcheck_newsletter_setting} WHERE add_this_newsletter = 1";
    $result = db_query($sql);	  
    $default_checked_boxes = array();
	  while ($data = db_fetch_object($result)) {
	    // turn all opt-out checkboxes' key to a nagative number
	    if ($data->opt_out >= 1) {
	      $newsletter_id_on_checkbox = -($data->newsletter_id);
	      $default_checked_boxes[] = -($data->newsletter_id);
	    } else {
	      $newsletter_id_on_checkbox = $data->newsletter_id;
	    }
	    $newsletters[$newsletter_id_on_checkbox] = $data->wording;
	  }    
  	  
    $form['simplecontactcheck_contact_form_newsletter'] = array(
      '#type' => 'checkboxes',
      '#options' => $newsletters,
      '#default_value' => $default_checked_boxes,
    );	  
	  
  	// move submit button to the bottom of the form
  	$form['submit'] = array(
  	  '#type'   => 'submit',
  	  '#value'  => t('Send e-mail'),
  	  '#weight' => 99999, // move submit button to the bottom of the form
  	);
  }
    
  
  // Inject HTML to Newsletter Settings page, and wire form submit process function
  // arg(4) is used to get newsletter id from query string like  
  elseif (($form_id == 'simplenews_admin_settings') && (($newsletter_id = arg(4)) >= 1)) {	
    // override the form submit process function while still execute original submit process
    // by appending function name to original array 'system_settings_form_submit' => array ( )
	  $form['#submit']['simplecontactcheck_newsletter_setting_submit'] = array();
	  
    // append validatation process to original validation process
    $form['#validate']['simplecontactcheck_simplenews_admin_settings_validate'] = array();	  
  	
	  // get setting data from DB
    $sql = "SELECT * FROM {simplecontactcheck_newsletter_setting} WHERE newsletter_id = %d";
    $result = db_query($sql, $newsletter_id);	  
	  if ($data = db_fetch_object($result)) {
	    $add_this_newsletter_dbValue = $data->add_this_newsletter;
	    $opt_out_dbValue = $data->opt_out;
	    $wording_dbValue = $data->wording;
	  }
	  
	  $form['simplecontactcheck_newsletter_setting'] = array(
	    '#type' => 'fieldset', 
	    '#title' => t('Contact Form Checkbox'), 
	    '#collapsible' => TRUE, 
	    '#collapsed' => FALSE,
	    '#weight' => 0, // to make it above the submit buttons
	  );  	
	  
	  $form['simplecontactcheck_newsletter_setting']['setting_group_description'] = array(
	  	'#type' => 'markup', 
	  	'#value' => t('These options determine if an option to subscribe to this newsletter will appear on the contact page of your website, whether that option is an opt-in or opt-out checkbox, and what the wording will be next to that checkbox on the contact form.'),
		'#prefix' => '<div class="description">',
  		'#suffix' => '</div>',
	  );
  	
    $form['simplecontactcheck_newsletter_setting']['add_this_newsletter'] = array(
      '#type'   => 'checkbox',
      '#title'  => t('Add this newsletter to the contact form'),
      '#default_value'  => $add_this_newsletter_dbValue,
    );
    
    $form['simplecontactcheck_newsletter_setting']['opt_out_setting'] = array(
      '#type'   => 'checkbox',
      '#title'  => t('Opt-out settings - check for opt-out / un-check for opt-in'),
      '#default_value'  => $opt_out_dbValue,
    );      
      
	  $form['simplecontactcheck_newsletter_setting']['opt_out_description'] = array(
	  	'#type' => 'markup', 
	  	'#value' => t('Newsletter subscription checkboxes are typically either opt-out or opt-in.  Opt-out means the checkbox is checked by default and the user is automatically subscribed to the newsletter unless they click on the checkbox, i.e. opt out, to remove the check.  Opt-in means they have to actively check the checkbox, or "opt in", in order to be subscribed to the checkbox.'),
		'#prefix' => '<div class="description" style="padding-left: 20px;">',
		'#suffix' => '</div>',
	  );      
	  
  	  $form['simplecontactcheck_newsletter_setting']['wording'] = array(
        '#type'   => 'textfield',
        '#title'  => t('Wording'),
        '#default_value'  => $wording_dbValue,
      );      
      
	  $form['simplecontactcheck_newsletter_setting']['wording_description'] = array(
	  	'#type' => 'markup', 
	  	'#value' => t('Enter the wording that you would like to appear to the right of the checkbox on the contact form. Typical wording includes "Yes, please subscribe me to the Adaptive Campus newsletter." Default is no wording, but then your users won\'t know what newsletter to subscribe to, so we recommend entering something simple.'),
		'#prefix' => '<div class="description" style="padding-left: 20px;">',
  		'#suffix' => '</div>',
	  );      	  
  }
}


function simplecontactcheck_newsletter_setting_submit($form_id, $form_values) {
  // get newsletter id from query string like
  // http://localhost:8244/drupal-5.10/?q=admin/content/newsletters/settings/3
  $newsletter_id = arg(4);
  
  $sql1 = "DELETE FROM {simplecontactcheck_newsletter_setting} WHERE newsletter_id = %d";
  
  db_query($sql1, $newsletter_id);   
  
  $sql2 = "INSERT {simplecontactcheck_newsletter_setting} SET newsletter_id = %d, add_this_newsletter = %d, opt_out  = %d, wording = '%s'";
  
  db_query($sql2, $newsletter_id, $form_values['add_this_newsletter'], $form_values['opt_out_setting'], $form_values['wording']);

  drupal_set_message(t('The Simplenews Settings have been saved.'));
}