Full Name of Module Project: Simplenews Contact Checkbox

Module Folder Name: simplecontactcheck

Basic Description: This is a small add-on module that works with the simplenews module to allow you to add checkboxes for newsletter susbscriptions to the contact form within drupal.   

User functionality: Based on the administrative settings, a checkbox for each newsletter should appear on the contact form just below the other form elements with an optional setting of wording next to each checkbox.  Only roles with viewing privileges should be able to see or use the checkboxes.

The checkboxes on the contact form with the the wording that is defined in the admin page to the left of the checkbox. If multiple newsletters are used, they would be listed one after the other.  

Each checkbox will be checked (opt-out) or un-checked (opt-in) based on the admin settings.

Administrative: When this module is enabled, a new fieldset form is displayed to people with the proper permissions to each newsletter settings under the Newsletter->Settings tab.

These options determine if an option to subscribe to this newsletter will appear on the contact page of your website, whether that option is an opt-in or opt-out checkbox, and what the wording will be next to that checkbox on the contact form.

The first option gives the administrator the option to add a checkbox to the contact form for the respective newsletter.

When checked, this would add the checkbox for this newsletter to the contact form just under the entry section of that form and only for the roles that have the access privileges to view/use the checkboxes which is defined under the permissions settings.

A second setting within the fieldset would allows them to select whether the checkbox is opt-in (default is an unchecked checkbox) or opt-out (default is a checked checkbox). If they check this box, meaning they are selecting ��opt-out��, then the checkbox for this newsletter on the contact form should be checked.  If they don��t check this box, meaning they are selecting ��opt-in��, then the checkbox for this newsletter on the contact form should be un-checked. 

The third field option allows the user to define the wording that appears next to the newsletter checkbox on the contact form. 

Permissions can be managed from the ��Access control�� areas, as it is with other drupal modules. 

Under the access control settings for this module, you will see a setting for which roles can see the checkboxes on the contact form. When this ��view contact checkboxes�� radio box is checked, that role can use the checkboxes on the contact form.